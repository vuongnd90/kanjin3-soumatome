package com.sva.n3.kanjin3.search;

import java.util.List;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.sva.n3.kanjin3.R;
import com.sva.n3.kanjin3.base.Debug;
import com.sva.n3.kanjin3.base.GlobalData;
import com.sva.n3.kanjin3.base.Word;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
 
public class SearchActivity extends ListActivity {
	private List<Word> words; 
	private String[] kanji;
	private String[] mean_china;
	private String[] mean_vi;
	EditText searchTo;

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
//		createDialogResult(words.get(position)).show();
		Intent intent = new Intent(getApplicationContext(), DetailWordActivity.class);
		GlobalData.curr_word_search = words.get(position);
		startActivity(intent);
		overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
		
	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.search_word_list);

        AdView adView = (AdView) findViewById(R.id.adViewSearch);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        words = GlobalData.getListWord();
        setListWordsAdapter(words);
        
        searchTo = (EditText)findViewById(R.id.inputSearch);
        searchTo.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            	words = GlobalData.getListWord(searchTo.getText().toString());
            	setListWordsAdapter(words);
            } 

        });
    }
    
    private void setListWordsAdapter(List<Word> words) {
    	mean_china = new String[words.size()];
        kanji = new String[words.size()];
        mean_vi = new String[words.size()];
        for(int index = 0; index < words.size(); index++){
      	  mean_china[index] = words.get(index).china_mean;
      	  kanji[index] = words.get(index).kanji;
      	  mean_vi[index] = words.get(index).vi_mean;
        }
        setListAdapter(new ListWordArrayAdapter(this, mean_china, kanji, mean_vi));
	}  
   
	private Dialog createDialogResult(Word word) {
		ContextThemeWrapper wrapper = new ContextThemeWrapper(this, android.R.style.Theme_Holo);
		LayoutInflater inflater = (LayoutInflater) wrapper.getSystemService(ContextThemeWrapper.LAYOUT_INFLATER_SERVICE);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(wrapper);
		View view = (View) inflater.inflate(R.layout.common_dialog_detail_word, null);
		alertDialogBuilder.setView(view);
		alertDialogBuilder.setCancelable(true);
//		alertDialogBuilder.setInverseBackgroundForced(true);		
		final Dialog alertDialog = alertDialogBuilder.create();
		alertDialog.setCanceledOnTouchOutside(false);
		
		TextView kanji = (TextView) view.findViewById(R.id.sw_kanji);
		TextView kunyoumi = (TextView) view.findViewById(R.id.sw_kunyoumi);
		TextView mean_vi = (TextView) view.findViewById(R.id.sw_mean_vi);
		TextView mean_china = (TextView) view.findViewById(R.id.sw_mean_china);
		TextView example = (TextView) view.findViewById(R.id.sw_example);
		TextView onyoumi = (TextView) view.findViewById(R.id.sw_onyoumi);
		
		kanji.setText(word.kanji);
		kunyoumi.setText(word.kun);
		mean_vi.setText(word.vi_mean);
		example.setText(word.vi_example);
		onyoumi.setText(word.on);
		mean_china.setText(word.china_mean);
		
		return alertDialog;
	}
}
