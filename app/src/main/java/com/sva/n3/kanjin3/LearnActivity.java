package com.sva.n3.kanjin3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.AdRequest;
import com.sva.n3.kanjin3.base.DatabaseHandler;
import com.sva.n3.kanjin3.base.Debug;
import com.sva.n3.kanjin3.base.GlobalData;
import com.sva.n3.kanjin3.base.Word;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.UserDictionary.Words;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.view.View;


public class LearnActivity extends Activity {
	
	ExpandableListAdapter listAdapter;
	ExpandableListView expListView;
	List<String> listDataHeader;
    HashMap<String, List<Word>> listDataChild;
    DatabaseHandler db;
    int curr_lesson = 1;
    int curr_word = 1;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_learn);

		db = GlobalData.db;
		
		expListView = (ExpandableListView) findViewById(R.id.lvExp);
		prepareListData();
		listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
		expListView.setAdapter(listAdapter);

		expListView.setOnChildClickListener(new OnChildClickListener() {
 
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                    int groupPosition, int childPosition, long id) {
            	Intent intent = new Intent(getApplicationContext(), WordActivity.class);
            	GlobalData.current_lesson = groupPosition;
            	GlobalData.current_word_in_lesson = childPosition;
            	startActivity(intent);
                return true;
            }
        });
	}
	
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<Word>>();

//        listDataHeader.add("All words");

        for (int i = 0; i < 36; i++){
            int week = (int)(i / 6) + 1;
            int day =  (i + 1) % 6 == 0 ? 6 : (i + 1) % 6;
        	listDataHeader.add("Week " + week + " - Day " + day);
        }

        List<Word> words = new ArrayList<Word>();
//        words = db.getAllWord("N5");
//        listDataChild.put(listDataHeader.get(0), words);
        // Adding child data
        for (int i = 0; i < 36; i++) {
            words = new ArrayList<Word>();
            int week = (int)(i / 6) + 1;
            int day =  (i + 1) % 6 == 0 ? 6 : (i + 1) % 6;
            words = db.getListWord(week, day);
            listDataChild.put(listDataHeader.get(i), words); // Header, Child data
        }
    }
}
