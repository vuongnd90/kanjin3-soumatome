package com.sva.n3.kanjin3.base;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

public class AddAdMob {
	AdView mAdView;
	LinearLayout layout;
	public AddAdMob(LinearLayout layout, Context context, String adID) {
		this.layout = layout;
		mAdView = new AdView(context);
		mAdView.setAdSize(AdSize.SMART_BANNER);
		mAdView.setAdUnitId(adID);
		
        AdRequest.Builder adRequest = new AdRequest.Builder();
//        adRequest.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        adRequest.addTestDevice("00868A588375BCF9D7364D369916C4FE");
//        adRequest.addTestDevice("4E26B8E9922CFC5F20C0394948725ED3");
        layout.addView(mAdView);
        mAdView.setVisibility(View.GONE);
        mAdView.loadAd(adRequest.build());
		
        mAdView.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
				// TODO Auto-generated method stub
				super.onAdLoaded();
				mAdView.setVisibility(View.VISIBLE);
			}
        	
        });
	}
}
