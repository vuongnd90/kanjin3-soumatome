package com.sva.n3.kanjin3;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

import com.sva.n3.kanjin3.base.Debug;
import com.sva.n3.kanjin3.base.GlobalData;
import com.sva.n3.kanjin3.base.Word;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WordDetailFragment extends Fragment{
    public final static String CURR_WORD ="CURR_WORD";
    private List<Word> words;
    private Context ctx;
    private LinearLayout word_dt;
    public Word word;
    public WebView webView;
    LinearLayout linearLayout;

    public static final WordDetailFragment newInstance(int index){
        WordDetailFragment fragment = new WordDetailFragment();
        Bundle bundle = new Bundle(1);
        bundle.putInt(CURR_WORD, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.ctx = container.getContext();
        int curr_word = getArguments().getInt(CURR_WORD);
        View view = inflater.inflate(R.layout.word_detail, container, false);

        int week = (int)(GlobalData.current_lesson / 6) + 1;
        int day =  (GlobalData.current_lesson + 1) % 6 == 0 ? 6 : (GlobalData.current_lesson + 1) % 6;
        if (GlobalData.current_lesson == 0)
            words = GlobalData.db.getAllWord("N5");
        else
            words = GlobalData.db.getListWord(week, day);

        word = words.get(curr_word);

        TextView kanji = (TextView) view.findViewById(R.id.wd_kanji);
//        TextView kunyoumi = (TextView) view.findViewById(R.id.wd_kunyoumi);
        TextView splelling = (TextView) view.findViewById(R.id.wd_spelling);
        TextView mean_vi = (TextView) view.findViewById(R.id.wd_mean_vi);
        TextView mean_china = (TextView) view.findViewById(R.id.wd_mean_china);
        TextView example = (TextView) view.findViewById(R.id.wd_example);
//        TextView onyoumi = (TextView) view.findViewById(R.id.wd_onyoumi);
        linearLayout = (LinearLayout) view.findViewById(R.id.layout_word_detail_draw_kanji);

        kanji.setText(word.kanji);
//        kunyoumi.setText(word.kun);
        splelling.setText(word.spelling);
        mean_vi.setText(word.vi_mean);
        example.setText(word.vi_example.replace("\\r\\n", System.getProperty("line.separator")));
//        example.setText(word.vi_example);
//        onyoumi.setText(word.on);
        mean_china.setText(word.china_mean);

        webView = (WebView) view.findViewById(R.id.drawKanji);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
//                String html = word.how_to_write.replaceAll("(\\r\\n|\\n)", "");
                String html = word.how_to_write;
                byte[] data = Base64.decode(html, Base64.DEFAULT);
                String text = "";
                try {
                    text = new String(data, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                html = text.replaceAll("(\\r\\n|\\n)", "");
                String javascript="javascript: document.getElementById('svg_content').innerHTML= '"+ html  +"';";
                if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    webView.loadUrl(javascript);
                } else {
                    view.evaluateJavascript(javascript, null);
                }
                super.onPageFinished(view, url);
            }
        });
        webView.loadUrl("file:///android_asset/template_kanji.html");

        // handle click event on button Redraw
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    webView.loadUrl("javascript:drawKanji()");
                } else {
                    webView.evaluateJavascript("javascript:drawKanji()", null);
                }
            }
        });

        word_dt = (LinearLayout) view.findViewById(R.id.word_dt);
        word_dt.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, WordActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                GlobalData.current_word_in_lesson =  getArguments().getInt(CURR_WORD);
                startActivity(intent);
            }
        });
        return view;
    }
}
