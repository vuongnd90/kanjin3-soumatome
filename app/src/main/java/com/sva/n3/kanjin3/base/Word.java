package com.sva.n3.kanjin3.base;

import android.R.bool;

public class Word {
	public int level;
	public String on;
	public String kanji;
	public String kun;
	public String china_mean;
	public String vi_mean;
	public String vi_example;
    public String spelling;
    public String how_to_write;
	public int lesson;
	public int order;
	public boolean isView = true;
	
	public Word() {
		
	}
}
