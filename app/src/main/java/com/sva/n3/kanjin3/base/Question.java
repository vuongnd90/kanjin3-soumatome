package com.sva.n3.kanjin3.base;

public class Question {
	public int id;
	public int kind;
	public String content;
    public String content_vi;
	public int result;
	public int choose;
	public String answer1;
	public String answer2;
	public String answer3 = "";
	public String answer4 = "";
	public String note;
	public int count;
	public String resource;
	public Question() {

	}
	
	public Question(int kind, String content, int result,
			String answer1, String answer2, String answer3, String answer4, String note,
			int count) {
		this.kind = kind;
		this.answer1 = answer1;
		this.answer2 = answer2;
		this.answer3 = answer3;
		this.answer4 = answer4;
		this.content = content;
		this.result = result;
		this.note = note;
		this.count = count;
	}

}
