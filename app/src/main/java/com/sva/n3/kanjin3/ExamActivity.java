package com.sva.n3.kanjin3;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.sva.n3.kanjin3.base.AdMob;
import com.sva.n3.kanjin3.base.Debug;
import com.sva.n3.kanjin3.base.GlobalData;
import com.sva.n3.kanjin3.base.Question;

import android.R.bool;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


public class ExamActivity extends Activity{
	Button answer1;
	Button answer2;
	Button answer3;
	Button answer4;
	TextView num_question;
	TextView content;
	Dialog result_dlg = null;
	List<Question> questions;
	int curr_question = 0;
	int correct = 0;
	int incorrect = 0;
    private int positionTrueAnswer;
	boolean isFirst = true;

    boolean flagTrue = false;

    AdMob adMob;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.exam_activity);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        adMob = new AdMob(this, getString(R.string.banner_ad_unit_id));

        answer1 = (Button) findViewById(R.id.answer1);
		answer2 = (Button) findViewById(R.id.answer2);
		answer3 = (Button) findViewById(R.id.answer3);
		answer4 = (Button) findViewById(R.id.answer4);
		
		content = (TextView) findViewById(R.id.content);
		num_question = (TextView) findViewById(R.id.ex_number_question);
		questions = GlobalData.getQuestion();
		
		loadNewQuestion();
		
		answer1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				handleAnswer(1);
			}
		});
		
		answer2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				handleAnswer(2);		
			}
		});
		
		answer3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				handleAnswer(3);
			}
		});
		
		answer4.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {	
				handleAnswer(4);
			}
		});
		
	}

    @Override
    protected void onDestroy() {
        adMob.showInterstitial();
        super.onDestroy();
    }

    private void handleAnswer(int selectedAnswer) {
        if (getFlagTrue() == true) {
            return;
        }
		// result = true if the answer is true else false
		Button trueButton;
		Button falseButton;
		if (selectedAnswer == questions.get(curr_question-1).result) {
            // set flagTrue is true
            setFlagTrue(true);
			if (isFirst)
				correct += 1;
			trueButton = getButtonFromId(selectedAnswer); 
			setAnimationForButton(this, trueButton, true);
			// load new question
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {	
					// load new question
					loadNewQuestion();
				}
			}, 1000);			
		} else {
//			trueButton = getButtonFromId(positionTrueAnswer);
			if (isFirst)
				incorrect += 1;
			falseButton = getButtonFromId(selectedAnswer);
			setAnimationForButton(this, falseButton, false);
		}		
		isFirst = false;
	}
	
	private SpannableStringBuilder processQuestion(String content){
		int b = content.indexOf("<u>");
		int e = content.indexOf("</u>");
		
		content.replace("<u>", "");
		content.replace("</u>", "");
		final SpannableStringBuilder sb = new SpannableStringBuilder(content);
		final ForegroundColorSpan fcs = new ForegroundColorSpan(Color.RED); 

		// Span to set text color to some RGB value
		final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); 

		// Span to make text bold
		sb.setSpan(fcs, b, e, Spannable.SPAN_INCLUSIVE_INCLUSIVE); 

		// Set the text color for first 4 characters
		sb.setSpan(bss, b, e, Spannable.SPAN_INCLUSIVE_INCLUSIVE); 

		return sb;
	}
	
	private void setStyleText(TextView tv, String text) {
//		if (text == "")
//			return;
//		int start = text.indexOf(new String("<u>"));
//		int end = text.lastIndexOf(new String("</u>")) - 3;
//		text = text.replace("<u>", "");
//		text = text.replace("</u>", "");
//		Spannable wordtoSpan = new SpannableString(text);
//		wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#d3414a")), start, end,
//				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//		final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
//		wordtoSpan.setSpan(bss, start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//		tv.setText(wordtoSpan);
        
        if (text == "")
            return;
        int start = text.indexOf(new String("<u>"));
        int end = text.lastIndexOf(new String("</u>")) - 3;
        text = text.replace("<u>", "");
        text = text.replace("</u>", "");
        Spannable wordtoSpan = new SpannableString(text);
        if (start >= 0 && end >= 0 && start < end)
            wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#d3414a")), start, end,
                               Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //		final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
        //		wordtoSpan.setSpan(bss, start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        tv.setText(wordtoSpan);
	}
	
	private void setTextForAnswers() {	
		if (curr_question >= GlobalData.QUESTION_LIMIT)
			return;
        // get the question
        Question question = questions.get(curr_question);
        // set current order of question
        num_question.setText(String.valueOf(curr_question+1) + ".");

        int number_of_answer = 4;
        Random random = new Random();
        Button button;

        ArrayList<String> wrongAnswers = new ArrayList<String>();
        // get old position of true answer
        int oldPositionTrueAnswer = question.result;
        for (int i = 1; i <= number_of_answer; i++) {
            if (i != oldPositionTrueAnswer) {
                wrongAnswers.add(getAnswerFromQuestion(question, i));
            }
        }
        // get new position for true answer
        positionTrueAnswer = random.nextInt(number_of_answer) + 1;
        // get true answer button
        button = getButtonFromId(positionTrueAnswer);
		// set text for true answer
        button.setText(getAnswerFromQuestion(question, oldPositionTrueAnswer));
        // set new position for true answer
        question.result = positionTrueAnswer;
        // set text for other button
        int index = 0;
        for (int i = 1; i <= number_of_answer; i++) {
            if (i != positionTrueAnswer) {
                button = getButtonFromId(i);
                button.setText(wrongAnswers.get(index));
                index++;
            }
        }
//		answer1.setText(question.answer1);
//		answer2.setText(question.answer2);
//		answer3.setText(question.answer3);
//		answer4.setText(question.answer4);
		
//		content.setText(processQuestion(question.content));
		setStyleText(content, question.content);
	}
	
	
	protected void loadNewQuestion() {
        // set flagTrue is false
        setFlagTrue(false);
		isFirst = true;
		if (curr_question == GlobalData.QUESTION_LIMIT && result_dlg == null) {
			result_dlg = createDialogResult();
			result_dlg.show();
		}
		
//		if (curr_question == 1) {
//			createDialogResult().show();
////			return;
//		}
		
		// set text for answer buttons
		setTextForAnswers();
		// increase value of count variable
		if (curr_question < 20)
			curr_question++;
	}
	
	private int getIdFormButton(String answer){
		switch (answer) {
		case "answer1":
			return 1;
		case "answer2":
			return 2;
		case "answer3":
			return 3;
		case "answer4":
			return 4;
		default:
			return 0;
		}
	}
	
	private Button getButtonFromId(int id){
		switch (id) {
		case 1:
			return answer1;
		case 2:
			return answer2;
		case 3:
			return answer3;
		case 4:
			return answer4;
		default:
			return null;
		}
	}
	
	
	public static void setAnimationForButton(Context context, Button button, boolean answer) {
		// True answer
		AnimationDrawable animation = new AnimationDrawable();
		if (answer == true) {
//			animation.addFrame(context.getResources().getDrawable(R.drawable.common_btn_border_true_answer), 100);
			animation.addFrame(context.getResources().getDrawable(R.drawable.btn_answer), 100);
			animation.addFrame(context.getResources().getDrawable(R.drawable.common_btn_border_true_answer), 100);
            animation.addFrame(context.getResources().getDrawable(R.drawable.btn_answer), 100);
		} else {
//			animation.addFrame(context.getResources().getDrawable(R.drawable.common_btn_border_false_answer), 100);
            animation.addFrame(context.getResources().getDrawable(R.drawable.btn_answer), 100);
			animation.addFrame(context.getResources().getDrawable(R.drawable.common_btn_border_false_answer), 100);
            animation.addFrame(context.getResources().getDrawable(R.drawable.btn_answer), 100);
		}
		animation.setOneShot(true);
		button.setBackgroundDrawable(animation);	
		animation.start();

	}

	private Dialog createDialogResult() {
		ContextThemeWrapper wrapper = new ContextThemeWrapper(this, android.R.style.Theme_Holo);
		LayoutInflater inflater = (LayoutInflater) wrapper.getSystemService(ContextThemeWrapper.LAYOUT_INFLATER_SERVICE);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(wrapper);
		View view = (View) inflater.inflate(R.layout.common_dialog_game_result, null);
		alertDialogBuilder.setView(view);
		alertDialogBuilder.setCancelable(true);
		alertDialogBuilder.setInverseBackgroundForced(true);		
		final Dialog alertDialog = alertDialogBuilder.create();
		alertDialog.setCanceledOnTouchOutside(false);
		alertDialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    finish();
                    alertDialog.dismiss();
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }
                return true;
            }
        });

		Button btnReLearn = (Button)view.findViewById(R.id.btn_replay);
		Button btnHome = (Button)view.findViewById(R.id.btn_home);
		Button btnShare = (Button)view.findViewById(R.id.btn_share);
		TextView tvCorrect = (TextView)view.findViewById(R.id.ex_tv_correct);
		TextView tvIncorrect = (TextView)view.findViewById(R.id.ex_tv_incorrect);
		
		tvCorrect.setText(String.valueOf(correct));
		tvIncorrect.setText(String.valueOf(incorrect));

		btnHome.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(ExamActivity.this, MainActivity.class));				
			}
		});
		
		btnReLearn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(ExamActivity.this, ExamActivity.class));				
			}
		});

		btnShare.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
								
			}
		});

		return alertDialog;
	}

    /**
     * Set value for flagTrue
     * @param value
     */
    private void setFlagTrue (boolean value) {
        flagTrue = value;
    }

    private boolean getFlagTrue() {
        return flagTrue;
    }

    private String getAnswerFromQuestion(Question question, int position) {
        String answer = "";
        switch (position) {
            case 1:
                answer =  question.answer1;
                break;
            case 2:
                answer =  question.answer2;
                break;
            case 3:
                answer =  question.answer3;
                break;
            case 4:
                answer =  question.answer4;
                break;
        }
        return answer;
    }
}
