package com.sva.n3.kanjin3;

import java.util.HashMap;
import java.util.List;

import com.sva.n3.kanjin3.base.Word;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class ExpandableListAdapter extends BaseExpandableListAdapter {
	private Context context;
	private List<String> listDataHeader;
	private HashMap<String, List<Word>> listDataChild;
	
	public ExpandableListAdapter(Context context, List<String> listDataHeader,
            HashMap<String, List<Word>> listChildData){
		this.context = context;
		this.listDataHeader = listDataHeader;
		this.listDataChild = listChildData;
	}
	
	@Override
	public int getGroupCount() {
		return listDataHeader.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return listDataChild.get(this.listDataHeader.get(groupPosition)).size();
	}
	
	@Override
	public Object getGroup(int groupPosition) {
		return this.listDataHeader.get(groupPosition);
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return this.listDataChild.get(this.listDataHeader.get(groupPosition)).get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}
	
	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		
		String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_lesson, null);
        }
 
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
 
        return convertView;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		final Word word = (Word) getChild(groupPosition, childPosition);
		
		if (convertView == null){
			LayoutInflater inflater =  (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.list_word_in_lesson, null);
		}
		
		TextView txtKanji = (TextView) convertView
                .findViewById(R.id.lblKanji);
		txtKanji.setText(word.kanji);
        
        TextView txtMean = (TextView) convertView
                .findViewById(R.id.lblMean);
        txtMean.setText(word.china_mean);
        
        return convertView;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
