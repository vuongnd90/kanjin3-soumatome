package com.sva.n3.kanjin3;

import com.sva.n3.kanjin3.base.AdMob;
import com.sva.n3.kanjin3.base.DatabaseHandler;
import com.sva.n3.kanjin3.base.GlobalData;
import com.sva.n3.kanjin3.search.SearchActivity;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends Activity {
	
    DatabaseHandler db;
    int curr_lesson = 1;
    int curr_word = 1;
    
    Button btnLearn;
	Button btnGame;
	Button btnOther;
	Button btnTest;

    AdMob adMob;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		adMob = new AdMob(this, getString(R.string.banner_ad_unit_id));
		db = GlobalData.openDatabase(this);
		
		btnLearn = (Button) findViewById(R.id.btn_learn);
		btnGame = (Button) findViewById(R.id.btn_game);
		btnOther = (Button) findViewById(R.id.btn_other);
		btnTest = (Button) findViewById(R.id.btn_test);
		
		btnLearn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getApplicationContext(), LearnActivity.class));
				overridePendingTransition(R.anim.fadein,R.anim.fadeout);
			}
		});
		
		btnTest.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				startActivity(new Intent(getApplicationContext(), ExamActivity.class));
                startActivity(new Intent(getApplicationContext(), SelectTestExerciseActivity.class));
				overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
			}
		});
		
		btnGame.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(getApplicationContext(), SearchActivity.class));
				overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
			}
		});

        btnOther.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://search?q=monosmartapp&c=apps"));
                startActivity(intent);
            }
        });
	}

    @Override
    protected void onDestroy() {
        adMob.showInterstitial();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle("Thông báo")
            .setMessage("Không học nữa à?")
            .setPositiveButton("Ừ, tàu lượn đây!", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
//	            System.exit(0);
                }
            })
            .setNegativeButton("Ở lại học tiếp đê!", null)
            .show();
    }
}
