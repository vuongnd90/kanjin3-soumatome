package com.sva.n3.kanjin3.base;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.internal.er;

/**
 * Created by vuongnd on 3/15/2015.
 */
public class AdMob {
    private static final String TAG = "KanjiN4";
    private InterstitialAd iAd;
    public  AdMob(final Context context, String AD_UNIT_ID) {
        iAd = new InterstitialAd(context);
        iAd.setAdUnitId(AD_UNIT_ID);

        iAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                Log.d(TAG, "onAdLoaded");
                Toast.makeText(context, "Load Ad successfully", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                String errorMessage = String.format("Failed to load add : "+ getErrorReason(errorCode));
                Log.d(TAG, errorMessage);
                Toast.makeText(context, "Load Ad fail " + getErrorReason(errorCode), Toast.LENGTH_SHORT).show();
            }
        });

        loadInterstitial();
    }

    public void loadInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("00868A588375BCF9D7364D369916C4FE")
                .build();
        iAd.loadAd(adRequest);
    }

    public void showInterstitial() {
        if (iAd.isLoaded()) {
            iAd.show();
        } else {
            Log.d(TAG, "Interstitial ad is not loaded yet");
        }
    }

    private String getErrorReason(int errorCode) {
        String errorReason = "unknown error";
        switch(errorCode) {
            case AdRequest.ERROR_CODE_INTERNAL_ERROR:
                errorReason = "internal error";
                break;
            case AdRequest.ERROR_CODE_INVALID_REQUEST:
                errorReason = "invalid request";
                break;
            case AdRequest.ERROR_CODE_NETWORK_ERROR:
                errorReason = "network Error";
                break;
            case AdRequest.ERROR_CODE_NO_FILL:
                errorReason = "no fill";
                break;
        }
        return errorReason;
    }

}
