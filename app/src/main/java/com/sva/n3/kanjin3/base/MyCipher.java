package com.sva.n3.kanjin3.base;

import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;

public class MyCipher {
 
    //Hàm chuyển đổi key là 1 chuỗi ký tự về 1 mảng byte làm làm chìa khóa mã hóa
    public static byte[] generateKey(String password) throws Exception {
        byte[] keyStart = password.getBytes();
        KeyGenerator kgen = KeyGenerator.getInstance("AES");
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "Crypto");
        sr.setSeed(keyStart);
        kgen.init(128, sr);
        SecretKey skey = kgen.generateKey();
        return skey.getEncoded();
    }
 
    //Hàm thực hiện việc mã hóa dữ liệu từ 1 key
    public static byte[] encodeFile(byte[] key, byte[] fileData) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(Base64.decode(fileData, 1));
        return encrypted;
    }
 
    //Hàm thực hiện việc giải mã dữ liệu từ 1 key
    public static byte[] decodeFile(byte[] key, byte[] fileData) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] decrypted = cipher.doFinal(fileData);
        return decrypted;
    }
 
}
