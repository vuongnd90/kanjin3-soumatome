package com.sva.n3.kanjin3;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.sva.n3.kanjin3.base.GlobalData;

/**
 * Created by vuongnd on 5/7/2015.
 */
public class SelectTestExerciseActivity extends Activity {

    Spinner spinnerWeek;
    Spinner spinnerDay;
    Button buttonStartTest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_exercise);
        spinnerWeek = (Spinner) findViewById(R.id.spinner_week);
        spinnerDay = (Spinner) findViewById(R.id.spinner_day);

        buttonStartTest = (Button) findViewById(R.id.btn_start_test);

        String[] weeks = new String[]{"1", "2", "3", "4", "5", "6"};
        String[] days = new String[]{"1", "2", "3", "4", "5", "6", "7"};

        ArrayAdapter<String> adapterWeek = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, weeks);
        spinnerWeek.setAdapter(adapterWeek);

        ArrayAdapter<String> adapterDay = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, days);
        spinnerDay.setAdapter(adapterDay);

        spinnerWeek.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                GlobalData.current_week = i + 1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                GlobalData.current_week = 1;
            }
        });

        spinnerDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                GlobalData.current_day = i + 1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                GlobalData.current_day = 1;
            }
        });

        buttonStartTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ExamN3Activity.class));
            }
        });
    }
}
