package com.sva.n3.kanjin3.base;

import java.util.List;

import android.content.Context;

public class GlobalData {
	public static final int LESSON_LIMIT = 11;
	public static final int QUESTION_LIMIT = 20;
	public static int current_lesson;
	public static int current_word_in_lesson;
    public static  int current_week;
    public static int current_day;
	public static Word curr_word_search;
	public static DatabaseHandler db;
	
	public static DatabaseHandler openDatabase(Context context){
		db = new DatabaseHandler(context);
		if (db.checkDatabaseExist() == false) {			
			db.checkAndCopyDatabase();
		}
		db.openDataBase();
		return db;
	}
	
	public static List<Question> getQuestion(){
		return db.getRandomListQuestion(QUESTION_LIMIT);
	}

    public static List<Question> getQuestionOfDay(int week, int day) {
        return db.getListQuestionOfDay(week, day);
    }
	
	public static List<Word> getListWord(){
		return db.getListWord();
	}
	
	public static List<Word> getListWord(String strFind){
		return db.getListWord(strFind);
	}
}
