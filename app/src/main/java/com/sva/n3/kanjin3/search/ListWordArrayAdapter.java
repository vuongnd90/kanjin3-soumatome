package com.sva.n3.kanjin3.search;

import com.sva.n3.kanjin3.R;
import com.sva.n3.kanjin3.base.Debug;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ListWordArrayAdapter extends ArrayAdapter<String> {
	private final Context context;
	private final String[] kanji;
	private final String[] mean_vi;
	private final String[] mean_china;
	
	
	public ListWordArrayAdapter(Context context, String[] mean_china, String[] kanji, String[] mean_vi) {
		super(context, R.layout.search_activity, mean_china);
		this.context = context;
		this.kanji = kanji;
		this.mean_vi = mean_vi;
		this.mean_china = mean_china;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.search_activity, parent, false);
		TextView tvMeanVi = (TextView) rowView.findViewById(R.id.search_tv_mean_vi);
		TextView tvKanji = (TextView) rowView.findViewById(R.id.search_tv_kanji);
		TextView tvMeanChi = (TextView) rowView.findViewById(R.id.search_tv_mean_china);
		tvMeanVi.setText(mean_vi[position]);
		if (kanji[position] != null)
			tvKanji.setText(kanji[position]);
		tvMeanChi.setText(mean_china[position]);

		return rowView;
	}
}
