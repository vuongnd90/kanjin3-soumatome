package com.sva.n3.kanjin3;

import java.util.ArrayList;
import java.util.List;

import com.sva.n3.kanjin3.base.AddAdMob;
import com.sva.n3.kanjin3.base.GlobalData;
import com.sva.n3.kanjin3.base.Word;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import android.widget.LinearLayout;

public class WordActivity extends FragmentActivity {
    private List<Word> words;
    private PageAdapter pageAdapter;
    ViewPager pager;
    AddAdMob addAddMod;
    LinearLayout layout;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.word_activity);

        layout = (LinearLayout) findViewById(R.id.activity_word_fragment_content);
        addAddMod = new AddAdMob(layout, this, getString(R.string.banner_ad_unit_id));

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        List<Fragment> fragments = getFragments();

        pageAdapter = new PageAdapter(getSupportFragmentManager(), fragments);

        pager = (ViewPager)findViewById(R.id.viewpager);
        pager.setAdapter(pageAdapter);
        pager.setCurrentItem(GlobalData.current_word_in_lesson, true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(getApplicationContext(), LearnActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private List<Fragment> getFragments(){
        List<Fragment> fList = new ArrayList<Fragment>();
        List<String> kanji = new ArrayList<String>();

        int week = (int)(GlobalData.current_lesson / 6) + 1;
        int day =  (GlobalData.current_lesson + 1) % 6 == 0 ? 6 : (GlobalData.current_lesson + 1) % 6;
        words = GlobalData.db.getListWord(week, day);

//        if (GlobalData.current_lesson < 0)
//            words = GlobalData.db.getAllWord("N5");
//        else
//            words = GlobalData.db.getListWord(GlobalData.current_lesson + 1);

        for (int i = 0; i < words.size(); i++){
            kanji.add(words.get(i).kanji);
        }

        for (int index = 0; index < kanji.size(); index++){
            fList.add(WordFragment.newInstance(kanji.get(index), index));
        }
        return fList;
    }

    private class PageAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments;

        public PageAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }
    }
}
