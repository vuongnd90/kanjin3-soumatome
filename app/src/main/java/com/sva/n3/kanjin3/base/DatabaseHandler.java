package com.sva.n3.kanjin3.base;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.sva.n3.kanjin3.base.Debug;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Base64;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "kanjin3.sqlite";
	private static final String WORD_TABLE = "kanji_n3";
	private static final String QUESTION_TABLE = "question_kanji_n4";
	private static final String DATABASE_PATH = "/data/data/com.sva.n3.kanjin3/databases/";
	private SQLiteDatabase myDB;
	private final Context ctx;
	public static final String TAG = "Database";
	
	private static final String ID = "id";
    private static final String QUESTION_ID = "question_id";
	private static final String LESSON = "lesson";
	private static final String LEVEL = "level";
	private static final String ONYOMI = "onyomi";
	private static final String KUNYOMI = "kunyomi";
    private static final String SPELLING = "spelling";
	private static final String KANJI = "kanji";
	private static final String VI_MEAN = "vi_mean";
	private static final String VI_EXAMPLE = "vi_example";
	private static final String CHINA_MEAN = "china_mean";
	private static final String ORDER = "num";
    private static final String HOWTOWRITE = "how_to_write";
	
	private static final String CONTENT = "question";
	private static final String KIND = "mondai";
	private static final String RESULT = "correct";
	private static final String CHOOSE = "choose";
	private static final String ANSWER1 = "answer1";
	private static final String ANSWER2 = "answer2";
	private static final String ANSWER3 = "answer3";
	private static final String ANSWER4 = "answer4";
	
	private static final String[] WORD_COLUMNS = { ID, LESSON, ONYOMI, KUNYOMI, KANJI, VI_MEAN, VI_EXAMPLE, CHINA_MEAN, ORDER};
	private static final String CREATE_TABLE_WORDS = "CREATE TABLE "
			+ WORD_TABLE + "("+ ID + " INTEGER, " + KANJI + " TEXT, " 
			+ ONYOMI + " TEXT, " + KUNYOMI + " TEXT, " + CHINA_MEAN + " TEXT," + LEVEL + " TEXT, "
			+ VI_MEAN + " TEXT, " + VI_EXAMPLE + " TEXT, " + LESSON + " INTEGER, " + ORDER + " INTEGER)";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.ctx = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_WORDS);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + WORD_TABLE);
		this.onCreate(db);
	}

	public synchronized void close() {
		if (myDB != null)
			myDB.close();
		super.close();
	}

	public boolean checkAndCopyDatabase() {
		boolean dbExist = checkDataBase();
		if (dbExist) {
			Log.d(TAG, "database already exist!");
			return true;
		} else {
			this.getReadableDatabase();
			try {
				copyDataBase();
			} catch (IOException e) {
				Log.d(TAG, "Error copying database");
			}
			return false;
		}
	}

	private boolean checkDataBase() {
		SQLiteDatabase checkDB = null;
		try {
			String myPath = DATABASE_PATH + DATABASE_NAME;
			checkDB = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READWRITE);
		} catch (SQLiteException e) {
		}
		if (checkDB != null)
			checkDB.close();
		return checkDB != null ? true : false;
	}
	
	public boolean checkDatabaseExist(){
		String myPath = DATABASE_PATH + DATABASE_NAME;
		File file = new File(myPath);
		if(file.exists()){
			return true;
		}else{
			return false;
		}
	}

	private void copyDataBase() throws IOException {
		InputStream myInput = ctx.getAssets().open(DATABASE_NAME);
		String outFileName = DATABASE_PATH + DATABASE_NAME;
		OutputStream myOutput = new FileOutputStream(outFileName);
		byte[] buffer = new byte[2 * 1024];
		int length;
		while ((length = myInput.read(buffer)) > 0) {
			myOutput.write(buffer, 0, length);
		}
		myOutput.flush();
		myOutput.close();
		myInput.close();
	}

	public SQLiteDatabase openDataBase() throws SQLException {
		String myPath = DATABASE_PATH + DATABASE_NAME;
		try {
			myDB = SQLiteDatabase.openDatabase(myPath, null,
					SQLiteDatabase.OPEN_READWRITE);
			return myDB;
		} catch (Exception e) {
			Debug.out("Can't open database");
			return null;
		}

	}
	
//	public List<Word> getListWord(int week, int day){
////		return getListWord(lessonID, "N4");
//        return getListWord(week, day);
//	}
	
	public String decodeData(byte[] data) {
		byte[] decodeString;
		try {
			decodeString = MyCipher.decodeFile(MyCipher.generateKey("MinhHoang2512"), data);
			return new String(decodeString, "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

    public List<Word> getAllWord(String level){
        /**
         * arg: kind = 1 if this word exist image (image file is not null) else 0 if this word doesn't image (image file is null )
         * return: List words in lesson
         */
        List<Word> words = new ArrayList<Word>();
        Cursor cursor = null;
        String sql = "";

        sql = "select * from " + WORD_TABLE;

        cursor = myDB.rawQuery(sql, null);

        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    Word word = new Word();
//					String kanji = cursor.getString(cursor.getColumnIndex(KANJI));
//					word.kanji = decodeData(Base64.decode(kanji, Base64.DEFAULT));
                    word.kanji = cursor.getString(cursor.getColumnIndex(KANJI));
//					word.on = cursor.getString(cursor.getColumnIndex(ONYOMI));
                    word.spelling = cursor.getString(cursor.getColumnIndex(SPELLING));
                    word.china_mean = cursor.getString(cursor.getColumnIndex(CHINA_MEAN));
                    word.vi_mean = cursor.getString(cursor.getColumnIndex(VI_MEAN));
                    word.vi_example = cursor.getString(cursor.getColumnIndex(VI_EXAMPLE));
                    word.lesson = cursor.getInt(cursor.getColumnIndex(LESSON));
                    word.how_to_write = cursor.getString(cursor.getColumnIndex(HOWTOWRITE));
//					word.order = cursor.getInt(cursor.getColumnIndex(ORDER));
                    words.add(word);
                } while (cursor.moveToNext());
            }
        }
        return words;
    }
	
	public List<Word> getListWord(int week, int day){
		/**
		 * arg: kind = 1 if this word exist image (image file is not null) else 0 if this word doesn't image (image file is null ) 
		 * return: List words in lesson
		 */
		List<Word> words = new ArrayList<Word>();
		Cursor cursor = null;
		String sql = "";
		
		sql = "select * from " + "kanji_n3" + " where week=" + week + " and day= '" + day + "'";
		
		cursor = myDB.rawQuery(sql, null);
		
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					Word word = new Word();
//					String kanji = cursor.getString(cursor.getColumnIndex(KANJI));
//					word.kanji = decodeData(Base64.decode(kanji, Base64.DEFAULT));
                    word.kanji = cursor.getString(cursor.getColumnIndex(KANJI));
//					word.on = cursor.getString(cursor.getColumnIndex(ONYOMI));
					word.spelling = cursor.getString(cursor.getColumnIndex(SPELLING));
					word.china_mean = cursor.getString(cursor.getColumnIndex(CHINA_MEAN));
					word.vi_mean = cursor.getString(cursor.getColumnIndex(VI_MEAN));
					word.vi_example = cursor.getString(cursor.getColumnIndex(VI_EXAMPLE));
					word.lesson = cursor.getInt(cursor.getColumnIndex(LESSON));
                    word.how_to_write = cursor.getString(cursor.getColumnIndex(HOWTOWRITE));
//					word.order = cursor.getInt(cursor.getColumnIndex(ORDER));
					words.add(word);
				} while (cursor.moveToNext());
			}
		}
		return words;
	}
	
	public List<Word> getListWord(){
		/**
		 * arg: kind = 1 if this word exist image (image file is not null) else 0 if this word doesn't image (image file is null ) 
		 * return: List words in lesson
		 */
		List<Word> words = new ArrayList<Word>();
		Cursor cursor = null;
		String sql = "";
		
		sql = "select * from " + WORD_TABLE;
		
		cursor = myDB.rawQuery(sql, null);
		
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					Word word = new Word();
					String kanji = cursor.getString(cursor.getColumnIndex(KANJI));
					word.kanji = decodeData(Base64.decode(kanji, Base64.DEFAULT));
					word.on = cursor.getString(cursor.getColumnIndex(ONYOMI));
					word.kun = cursor.getString(cursor.getColumnIndex(KUNYOMI));
					word.china_mean = cursor.getString(cursor.getColumnIndex(CHINA_MEAN));
					word.vi_mean = cursor.getString(cursor.getColumnIndex(VI_MEAN));
					word.vi_example = cursor.getString(cursor.getColumnIndex(VI_EXAMPLE));
					word.lesson = cursor.getInt(cursor.getColumnIndex(LESSON));
					words.add(word);
				} while (cursor.moveToNext());
			}
		}
		return words;
	}
	
	public List<Word> getListWord(String strFind){
		/**
		 * arg: kind = 1 if this word exist image (image file is not null) else 0 if this word doesn't image (image file is null ) 
		 * return: List words in lesson
		 */
		List<Word> words = new ArrayList<Word>();
		Cursor cursor = null;
		String sql = "";
		
		sql = "select * from " + WORD_TABLE + " where china_mean like " + "'%" + strFind +"%' or vi_mean like " + "'%" + strFind +"%'";
		
		cursor = myDB.rawQuery(sql, null);
		
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					Word word = new Word();
					String kanji = cursor.getString(cursor.getColumnIndex(KANJI));
					word.kanji = decodeData(Base64.decode(kanji, Base64.DEFAULT));
					word.on = cursor.getString(cursor.getColumnIndex(ONYOMI));
					word.kun = cursor.getString(cursor.getColumnIndex(KUNYOMI));
					word.china_mean = cursor.getString(cursor.getColumnIndex(CHINA_MEAN));
					word.vi_mean = cursor.getString(cursor.getColumnIndex(VI_MEAN));
					word.vi_example = cursor.getString(cursor.getColumnIndex(VI_EXAMPLE));
					word.lesson = cursor.getInt(cursor.getColumnIndex(LESSON));
//					word.order = cursor.getInt(cursor.getColumnIndex(ORDER));
					words.add(word);
				} while (cursor.moveToNext());
			}
		}
		return words;
	}
	
	public List<String> getListKanji(int lessonID){
		/**
		 * arg: kind = 1 if this word exist image (image file is not null) else 0 if this word doesn't image (image file is null ) 
		 * return: List words in lesson
		 */
		List<String> words = new ArrayList<String>();
		Cursor cursor = null;
		String sql = "";
		String kanji;
		sql = "select * from " + WORD_TABLE + " where lesson=" + lessonID;
		
		cursor = myDB.rawQuery(sql, null);
		
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					kanji = cursor.getString(cursor.getColumnIndex(KANJI));
					words.add(decodeData(Base64.decode(kanji, Base64.DEFAULT)));
				} while (cursor.moveToNext());
			}
		}
		return words;
	}
	
	public List<String> getListKanji(){
		/**
		 * arg: kind = 1 if this word exist image (image file is not null) else 0 if this word doesn't image (image file is null ) 
		 * return: List words in lesson
		 */
		List<String> words = new ArrayList<String>();
		Cursor cursor = null;
		String sql = "";
		String kanji;
		sql = "select * from " + WORD_TABLE;
		
		cursor = myDB.rawQuery(sql, null);
		
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					kanji = cursor.getString(cursor.getColumnIndex(KANJI));
					words.add(decodeData(Base64.decode(kanji, Base64.DEFAULT)));
				} while (cursor.moveToNext());
			}
		}
		return words;
	}
	
	public List<Question> getRandomListQuestion(){
		return getRandomListQuestion(20);
	}

    public List<Question> getListQuestionOfDay(int week, int day){
        List<Question> questions = new ArrayList<Question>();
        Cursor cursor = null;
        String sql = "select * from question where week = " + week + " and day = " + day + " order by random()";
        Debug.out(sql);
        cursor = myDB.rawQuery(sql, null);

        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    Question question = new Question();
                    question.content = cursor.getString(cursor.getColumnIndex("content"));
                    question.content_vi = cursor.getString(cursor.getColumnIndex("content_vi"));
                    int question_id = cursor.getInt(cursor.getColumnIndex(QUESTION_ID));
                    String sql1 = "select * from answer where question_id =" + question_id;
                    Cursor cursor1 = null;
                    cursor1 = myDB.rawQuery(sql1, null);
                    int count = 0;
                    if (cursor1 != null && cursor1.getCount() > 0) {
                        if (cursor1.moveToFirst()) {
                            do {
                                count++;
                                switch (count) {
                                    case 1:
                                        question.answer1 = cursor1.getString(cursor1.getColumnIndex("content"));
                                        if (cursor1.getInt(cursor1.getColumnIndex("is_answer")) == 1) {
                                            question.result = 1;
                                        }
                                        break;
                                    case 2:
                                        question.answer2 = cursor1.getString(cursor1.getColumnIndex("content"));
                                        if (cursor1.getInt(cursor1.getColumnIndex("is_answer")) == 1) {
                                            question.result = 2;
                                        }
                                        break;
                                    case 3:
                                        question.answer3 = cursor1.getString(cursor1.getColumnIndex("content"));
                                        if (cursor1.getInt(cursor1.getColumnIndex("is_answer")) == 1) {
                                            question.result = 3;
                                        }
                                        break;
                                    case 4:
                                        question.answer4 = cursor1.getString(cursor1.getColumnIndex("content"));
                                        if (cursor1.getInt(cursor1.getColumnIndex("is_answer")) == 1) {
                                            question.result = 4;
                                        }
                                        break;
                                }
                            } while (cursor1.moveToNext());
                        }
                    }
                    questions.add(question);
                } while (cursor.moveToNext());
            }
        }
        return questions;
    }

	public List<Question> getRandomListQuestion(int limit){
		List<Question> questions = new ArrayList<Question>();
		Cursor cursor = null;
		String sql = "select * from " + QUESTION_TABLE +  " order by random() limit " + limit;
		
		cursor = myDB.rawQuery(sql, null);
		
		if (cursor != null && cursor.getCount() > 0) {
			if (cursor.moveToFirst()) {
				do {
					Question question = new Question();
					question.kind = cursor.getInt(cursor.getColumnIndex(KIND));
					question.content = cursor.getString(cursor.getColumnIndex(CONTENT));
					question.answer1 = cursor.getString(cursor.getColumnIndex(ANSWER1));
					question.answer2 = cursor.getString(cursor.getColumnIndex(ANSWER2));
					question.answer3 = cursor.getString(cursor.getColumnIndex(ANSWER3));
					question.answer4 = cursor.getString(cursor.getColumnIndex(ANSWER4));
					question.result = cursor.getInt(cursor.getColumnIndex(RESULT));
					questions.add(question);
				} while (cursor.moveToNext());
			}
		}
		return questions;
	}

    public List<Question> getRandomListQuestionN5(int limit){
        List<Question> questions = new ArrayList<Question>();
        Cursor cursor = null;
        String sql = "select * from " + QUESTION_TABLE +  " order by random() limit " + limit;

        cursor = myDB.rawQuery(sql, null);

        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    Question question = new Question();
                    question.kind = cursor.getInt(cursor.getColumnIndex(KIND));
                    question.content = cursor.getString(cursor.getColumnIndex(CONTENT));
                    String option = cursor.getString(cursor.getColumnIndex("options"));
                    String answer[] = option.split(",");
                    question.answer1 = cursor.getString(cursor.getColumnIndex(ANSWER1));
                    question.answer2 = cursor.getString(cursor.getColumnIndex(ANSWER2));
                    question.answer3 = cursor.getString(cursor.getColumnIndex(ANSWER3));
                    question.answer4 = cursor.getString(cursor.getColumnIndex(ANSWER4));
                    question.result = cursor.getInt(cursor.getColumnIndex(RESULT));
                    questions.add(question);
                } while (cursor.moveToNext());
            }
        }
        return questions;
    }
	
	/**
	 * for calculate number of lessons
	 * @return
	 */
	public int NumberOfLesson() {
		String sql = "select COUNT (*) from " + WORD_TABLE;
		Cursor cursor = myDB.rawQuery(sql, null);
		cursor.moveToFirst();
	    int count = cursor.getInt(0);
		cursor.close();
		return count;
	}

}