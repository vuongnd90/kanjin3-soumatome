package com.sva.n3.kanjin3.search;
import com.sva.n3.kanjin3.R;
import com.sva.n3.kanjin3.base.Debug;
import com.sva.n3.kanjin3.base.GlobalData;
import com.sva.n3.kanjin3.base.Word;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class DetailWordActivity extends Activity{
	
	TextView tvkanji;
	TextView tvkunyoumi;
	TextView tvmean_vi;
	TextView tvmean_china;
	TextView tvexample;
	TextView tvonyoumi;
    LinearLayout linearLayout;
	WebView webView;
    Button btnRedraw;
    Word word;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.common_dialog_detail_word);
		
		tvkanji = (TextView) findViewById(R.id.sw_kanji);
		tvkunyoumi = (TextView) findViewById(R.id.sw_kunyoumi);
		tvmean_vi = (TextView) findViewById(R.id.sw_mean_vi);
		tvmean_china = (TextView) findViewById(R.id.sw_mean_china);
		tvexample = (TextView) findViewById(R.id.sw_example);
		tvonyoumi = (TextView) findViewById(R.id.sw_onyoumi);
        linearLayout = (LinearLayout) findViewById(R.id.layout_draw_kanji);
		
		word = GlobalData.curr_word_search;
		tvkanji.setText(word.kanji);
		tvkunyoumi.setText(word.kun);
		tvmean_vi.setText(word.vi_mean);
		tvexample.setText(word.vi_example);
		tvonyoumi.setText(word.on);
		tvmean_china.setText(word.china_mean);

        webView = (WebView) findViewById(R.id.drawKanji);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                InputStream inputStream;
                String html = "";
                try {
                    Debug.out(String.format("\\%04x", (int) word.kanji.charAt(0)).substring(1));
                    inputStream = getAssets().open(String.format("\\%04x", (int) word.kanji.charAt(0)).substring(1) + ".html");
                    int size = inputStream.available();
                    byte[] buffer = new byte[size];
                    inputStream.read(buffer);
                    inputStream.close();
                    html = new String(buffer);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                html = html.replaceAll("(\\r\\n|\\n)", "");
                String javascript="javascript: document.getElementById('svg_content').innerHTML= '"+ html +"';";
                if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    webView.loadUrl(javascript);
                } else {
                    view.evaluateJavascript(javascript, null);
                }
                Debug.log(view);
                super.onPageFinished(view, url);
            }
        });
        webView.loadUrl("file:///android_asset/template_kanji.html");
        // handle click event on button Redraw
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                webView.loadUrl("javascript:drawKanji()");
                if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    webView.loadUrl("javascript:drawKanji()");
                } else {
                    webView.evaluateJavascript("javascript:drawKanji()", null);
                }
            }
        });
	}
}
