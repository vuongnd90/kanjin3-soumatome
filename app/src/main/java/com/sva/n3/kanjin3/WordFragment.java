package com.sva.n3.kanjin3;

import java.util.List;

import com.sva.n3.kanjin3.base.GlobalData;
import com.sva.n3.kanjin3.base.Word;

import android.view.View.OnClickListener;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WordFragment extends Fragment {
    public static final String KANJI = "KANJI";
    public static final String WORD ="CURR_WORD";
    public static Context ctx;
    private List<Word> words;
    private LinearLayout word_view;

    public static final WordFragment newInstance(String kanji, int curr_word){
        WordFragment fragment = new WordFragment();
        Bundle bundle = new Bundle(1);
        bundle.putString(KANJI, kanji);
        bundle.putInt(WORD, curr_word);
        fragment.setArguments(bundle);
        return fragment;
    }

    public void setViewWord(View view, Word word){
        LinearLayout vg = (LinearLayout) view.findViewById(R.id.word_dt);
        word.isView = true;
        disableEnableControls(false, vg);
    }

    private void disableEnableControls(boolean enable, ViewGroup vg){
        for (int i = 0; i < vg.getChildCount(); i++){
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup){
                disableEnableControls(enable, (ViewGroup)child);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        String txt = getArguments().getString(KANJI);
        this.ctx = container.getContext();
        final View view = inflater.inflate(R.layout.word_fragment, container, false);

        word_view = (LinearLayout) view.findViewById(R.id.word_frg);
        TextView txtKanji = (TextView) view.findViewById(R.id.txtKanji);
        txtKanji.setText(txt);

        word_view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, WordDetail.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                int curr_word = getArguments().getInt(WORD);
                GlobalData.current_word_in_lesson =  curr_word;
                startActivity(intent);
            }
        });
        return view;
    }
}
